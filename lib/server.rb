module LiquidInstaller
  module ServerCore
    include WEBrick
    
    class ActionCancel < HTTPServlet::AbstractServlet
      def initialize(server, win, mainServ)
        super server
        
        @win = win
        @mainServ = mainServ
      end
      
      def do_GET(req,resp)
        Gtk.query do
          dlg = Gtk::MessageDialog.new(@win,Gtk::Dialog::MODAL|Gtk::Dialog::DESTROY_WITH_PARENT,Gtk::MessageDialog::QUESTION,Gtk::MessageDialog::BUTTONS_YES_NO,"Are you sure you want to abort the installation?")
          res = dlg.run
          if res == Gtk::Dialog::RESPONSE_YES
            Gtk::GTK_PENDING_BLOCKS_LOCK.synchronize do
              @mainServ.stop
              Gtk.main_quit
              exit!
            end
          end
        end
        resp.body = ""
        resp['Content-Type'] = 'text/plain'
      end
    end
    
    class Server
      attr_reader :win, :thread, :s
      def initialize(win)
        @win = win
        @thread = Thread.new(self) do |server|
          @s = WEBrick::HTTPServer.new(:Port => 26888)
          @s.mount "/", WEBrick::HTTPServlet::FileHandler, "html"
          @s.mount "/action/cancel", ActionCancel, server.win, server
          @s.start
        end
      end
      
      def stop
        Mutex.new.synchronize do
          @s.stop
          Thread.kill(@thread)
        end
      end
    end
  end
end